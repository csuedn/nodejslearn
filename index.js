const express = require("express");
const app = express();

/*
mongoose = require("mongoose");
Task = require("./api/models/models.js");
bodyParser = require('body-parser');

mongoose.Promise = global.Promise;
mongoose.connect('mongodb://localhost/Tododb');

app.use(bodyParser.urlencoded({extended:true}));
app.use(bodyParser.json());

var routes = require('./api/routes/routes.js');
routes(app)
*/

app.listen(8000, () => {
    console.log("App is running at 8000");
});
